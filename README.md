## A character suggester for City of Heroes

Sometimnes it can be difficult to figure out what alt you want to make next. This web site gives you some
suggestions based on themes, and then shows you archetypes and power sets that fit those themes.

You can access the official home of this app at https://coh.tips/

## Building

The site is built using [Node.js](https://nodejs.org/en/) (LTS) and [Webpack](https://webpack.js.org/). You can get started developing locally with:

```
npm install
npm run build
npm start
```

And then visit http://localhost:9000/ to load your local copy of the site.

Re-running `npm run build` will populate the `dist` folder with a set of HTML and asset files that can be uploaded to a web server. The application itself runs entirely client-side in the browser.

You will also want to familiarize yourself with [Knockout](https://knockoutjs.com/) if you want to modify the UI. It's used to handle automatic data binding to the HTML.

## Structure

The main files are in these folders:

* [dist/index.html](dist/index.html) - This is the only file in the `dist` folder that should be modified directly. It is the base of the single-page application.
* [fonts](fonts) - This folder contains the custom font files.
* [images](images) - This folder contains the archetype icons used in the UI. (Hi-rez images kindly provided by [Rylas](https://forums.homecomingservers.com/topic/8106-want-a-higher-res-version-of-a-power-icon/)!)
* [sass](sass) - This folder contains the source for the style sheets used by the UI, written in [SASS](https://sass-lang.com/). The page also use a reset sheet known as [Normalize.css](https://github.com/JohnAlbin/normalize-scss).
* [src](src) - This folder contains all of the JavaScript application code.

## License

The application is distributed under an MIT license. You're welcome to copy, modify, and set up your own site if you want, as long as you follow the rules of the license. Refer to the [license file](LICENSE.md) for more information.