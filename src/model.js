import * as suggest from "./suggest";
import * as namegen from "./namegen";
import ko from "./knockout-3.5.0";
import ui from "../data/ui.json";
import * as prefs from "./userprefs";
import "../sass/model.scss";

const DEFAULT_ISSUE = "i27";
const DEFAULT_ROLLS = 5;
const DEFAULT_LIMIT_RESULTS = true;
const DEFAULT_ATTEMPTS = 3;
const DEFAULT_TAB = "suggestions";
const NAMES_TAB = "names";
const DEFAULT_NAMES_GENDER = "";
const DEFAULT_NAMES_ROLLS = 21;

const PREF_KEY_EXCLUDES = "exclusions_";
const PREF_KEY_THEME_EXCLUDES = "theme_excludes";
const PREF_KEY_ISSUE = "issue2";
const PREF_KEY_LIMIT_RESULTS = "limit_results";
const PREF_KEY_ROLLS = "rolls";
const PREF_KEY_NAMES_GENDER = "gender";
const PREF_KEY_NAMES_ROLLS = "name_rolls";

function get_starting_tab()
{
    if (document.location.hash === "#" + NAMES_TAB) {
        return NAMES_TAB;
    }
    return DEFAULT_TAB;
}

let cityViewModel = {
    "issue": ko.observable(prefs.get(PREF_KEY_ISSUE, DEFAULT_ISSUE)),
    "rolls": ko.observable(prefs.get_int(PREF_KEY_ROLLS, DEFAULT_ROLLS)),
    "pri_theme": ko.observable(null),
    "pri_theme_locked": ko.observable(false),
    "user_pri_theme": ko.observable(null),
    "sec_theme": ko.observable(null),
    "sec_theme_locked": ko.observable(false),
    "user_sec_theme": ko.observable(null),
    "theme_name_suggested": ko.observable(""),
    "archetypes": ko.observableArray(),
    "config_archetypes": ko.observableArray(),
    "themes": ko.observableArray(),
    "config_themes": ko.observableArray(),
    "sets": ko.observableArray(),
    "error": ko.observable(""),
    "selected_tab": ko.observable(get_starting_tab()),
    "limit_results": ko.observable(prefs.get_bool(PREF_KEY_LIMIT_RESULTS, DEFAULT_LIMIT_RESULTS)),
    "all_ats_excluded": ko.observable(false),
    "all_themes_excluded": ko.observable(false),
    "open_theme_chooser": ko.observable(false),
    "names_gender": ko.observable(prefs.get(PREF_KEY_NAMES_GENDER, DEFAULT_NAMES_GENDER)),
    "names": ko.observableArray(),
    "name_pri_theme": ko.observable(null),
    "name_sec_theme": ko.observable(null),
    "name_rolls": ko.observable(prefs.get(PREF_KEY_NAMES_ROLLS, DEFAULT_NAMES_ROLLS)),

    "exclude_at": function (at) {
        suggest.set_exclude_at(at.at, at.exclude);
        this.config_archetypes.removeAll();
        this.config_archetypes(suggest.get_all_ats());
    },

    "exclude_theme": function(theme) {
        suggest.set_exclude_theme(theme.th, theme.exclude);
        this.config_themes.removeAll();
        this.config_themes(suggest.get_all_themes());
    },

    "get_sets": function (theme) {
        let sets = new Set();
        [...this.sets()].filter(set => set.themes.some(th => th === theme)).forEach(value => sets.add(value.name));
        return [...sets.values()].sort();
    },

    "get_at_desc": function (archetype) {
        if (archetype) {
            let ui_at = ui.archetypes.find(at => at.at === archetype.at);
            if (ui_at) {
                return ui_at.desc;
            }
            this.error("Unable to find description for archetype " + archetype.name);
        }
        return "";
    },

    "get_theme_desc": function (theme) {
        if (theme) {
            let ui_theme = ui.themes.find(th => th.th === theme.th);
            if (ui_theme) {
                return ui_theme.desc;
            }
            this.error("Unable to find description for theme " + theme.name);
        }
        return "";
    },

    "roll_random_at": function () {
        this.error("");
        try {
            let results = [];
            let i = 0;
            while (i < this.rolls()) {
                let roll = suggest.roll_a_random_at_and_sets();
                let existing_at = results.find(result => result.archetype.at === roll.archetype.at);
                if (existing_at) {
                    if (existing_at.sets.some(s => { return s.primary.name === roll.sets[0].primary.name && s.secondary.name === roll.sets[0].secondary.name; })) {
                        continue;
                    }
                    existing_at.sets = existing_at.sets.concat(roll.sets);
                } else {
                    results.push(roll);
                }
                i++;
            }
            results.sort(archetype_comparer);
            this.pri_theme(null)
                .pri_theme_locked(false)
                .sec_theme(null)
                .sec_theme_locked(false)
                .theme_name_suggested("");
            this.archetypes.removeAll();
            this.archetypes(results);
        } catch (e) {
            this.error(e);
        }
    },

    "roll_random_themes": function () {
        this.error("");
        try {
            let options = {
                suggest_only_one_set: this.limit_results(),
            };
            if (this.pri_theme_locked() && this.pri_theme()) {
                options.pri_theme = this.pri_theme().th;
            }
            if (this.sec_theme_locked() && this.sec_theme()) {
                options.sec_theme = this.sec_theme().th;
            }
            for (var i = 0; i < DEFAULT_ATTEMPTS; i++) {
                let result = suggest.roll_random_themes(options);
                if (result.archetypes.length > 0) {
                    this.pri_theme(result.pri_theme);
                    this.sec_theme(result.sec_theme);
                    result.archetypes.sort(archetype_comparer);
                    this.archetypes.removeAll();
                    this.archetypes(result.archetypes);
                    this.roll_random_theme_name();
                    return;
                }
            }

            this.pri_theme(null)
                .pri_theme_locked(false)
                .sec_theme(null)
                .sec_theme_locked(false)
                .theme_name_suggested("");
            this.archetypes.removeAll();
            this.error("Unable to generate results. Do you have too many exclusions?");
        } catch (e) {
            this.error(e);
        }
    },

    "roll_random_names": function() {
        let options = {
            "gender": this.names_gender(),
            "rolls": this.name_rolls(),
        };
        if (this.name_pri_theme()) {
            options.pri_theme = this.name_pri_theme();
        }
        if  (this.name_sec_theme()) {
            options.sec_theme = this.name_sec_theme();
        }
        let result = namegen.roll_random_names(options);
        this.names.removeAll();
        this.names(result.names);
    },

    "roll_random_theme_name": function() {
        if (this.pri_theme() && this.sec_theme()) {
            let result = namegen.roll_random_names({
                "gender": this.names_gender(),
                "rolls": 1,
                "pri_theme": this.pri_theme(),
                "sec_theme": this.sec_theme()
            });
            this.theme_name_suggested(result.names.length ? result.names[0] : "");
        } else {
            this.theme_name_suggested("");
        }
    },

    "pass_themes_to_namegen": function() {
        if (this.pri_theme()) {
            this.name_pri_theme(this.pri_theme());
        }
        if (this.sec_theme()) {
            this.name_sec_theme(this.sec_theme());
        }
        this.roll_random_names();
        this.selected_tab("names");
    },

    "post_theme_name_update": function() {
        if (tooltips) {
            tooltips.add(".help");
        }
    },

    "is_tab_selected": function (tab) {
        return tab === this.selected_tab() ? "-selected" : "";
    },

    "themes_are_different": function () {
        let pri_th = this.pri_theme() ? this.pri_theme().th : "";
        let sec_th = this.sec_theme() ? this.sec_theme().th : "";
        return pri_th !== sec_th;
    },

    "post_at_update": function () {
        if (tooltips) {
            tooltips.add(".help");
        }
        let results = document.getElementById("preresults");
        if (results) {
            results.scrollIntoView({ behavior: "smooth", block: "center" });
        }
    },

    "post_names_update": function() {
        let results = document.getElementById("prenameresults");
        if (results) {
            results.scrollIntoView({ behavior: "smooth", block: "center" });
        }
    },

    "reset_excluded_ats": function () {
        preload_issue(this.issue(), false);
        reset_post_issue();
    },

    "reset_excluded_themes": function() {
        suggest.reset_excluded_themes();
        this.config_themes.removeAll();
        this.config_themes(suggest.get_all_themes());
    },

    "toggle_pri_theme": function () {
        this.pri_theme_locked(!this.pri_theme_locked());
    },

    "toggle_sec_theme": function () {
        this.sec_theme_locked(!this.sec_theme_locked());
    },

    "use_selected_themes": function () {
        if (this.user_pri_theme()) {
            this.pri_theme(this.user_pri_theme());
            this.pri_theme_locked(true);
        } else {
            this.pri_theme(null);
            this.pri_theme_locked(false);
        }
        if (this.user_sec_theme()) {
            this.sec_theme(this.user_sec_theme());
            this.sec_theme_locked(true);
        } else {
            this.sec_theme(null);
            this.sec_theme_locked(false);
        }
        this.open_theme_chooser(false);
        this.roll_random_themes();
    },

    "string_color": function(str) {
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
          hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }

        let r = hash >> 16 & 0xff;
        let g = hash >> 8 & 0xff;
        let b = hash & 0xff;
        while (color_too_dark(r, g, b)) {
            r += 20;
            g += 20;
            b += 20;
        }

        return "rgba(" + r  + ", " + g + ", " + b + ", 255)";
    }
}

function color_too_dark(r, g, b) {
    // convert RGB to YIQ to get an idea of how dark or light the color is
    let yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    return (yiq < 128);
}
  

function archetype_comparer(a, b) {
    if (a.archetype.name < b.archetype.name) {
        return -1;
    }
    if (a.archetype.name > b.archetype.name) {
        return 1;
    }
    return 0;
}

function preload_issue(issue, restore) {
    suggest.preload_issues(issue);
    if (restore) {
        restore_exclusions();
    }
}

function reset_post_issue() {
    cityViewModel.config_archetypes.removeAll();
    cityViewModel.config_archetypes(suggest.get_all_ats());

    cityViewModel.themes.removeAll();
    cityViewModel.themes(suggest.get_all_themes());

    cityViewModel.config_themes.removeAll();
    cityViewModel.config_themes(suggest.get_all_themes());

    cityViewModel.sets.removeAll();
    cityViewModel.sets(suggest.get_all_sets());

    cityViewModel.archetypes.removeAll();

    cityViewModel.pri_theme(null)
        .pri_theme_locked(false)
        .sec_theme(null)
        .sec_theme_locked(false)
        .theme_name_suggested("")
        .error("");
}

function restore_exclusions() {
    let ats = prefs.get_obj(PREF_KEY_EXCLUDES + cityViewModel.issue(), []);
    for (var at of ats) {
        suggest.set_exclude_at(at.at, at.exclude);
    }
    let themes = prefs.get_obj(PREF_KEY_THEME_EXCLUDES, []);
    for (var theme of themes) {
        suggest.set_exclude_theme(theme.th, theme.exclude);
    }
}

cityViewModel.issue.subscribe(function (new_issue) {
    preload_issue(new_issue, true);
    prefs.save(PREF_KEY_ISSUE, new_issue);
    reset_post_issue();
});

cityViewModel.config_archetypes.subscribe(function (new_ats) {
    if (new_ats.length > 0) {
        prefs.save_obj(PREF_KEY_EXCLUDES + cityViewModel.issue(), new_ats.map(at => {
            return { "at": at.at, "exclude": at.exclude };
        }));
        cityViewModel.all_ats_excluded(
            new_ats.every(at => at.exclude)
        );
    }
});

cityViewModel.config_themes.subscribe(function (new_themes) {
    if (new_themes.length > 0) {
        prefs.save_obj(PREF_KEY_THEME_EXCLUDES, new_themes.map(th => {
            return { "th": th.th, "exclude": th.exclude };
        }));
        cityViewModel.all_themes_excluded(
            new_themes.every(th => th.exclude)
        );
    }
});

cityViewModel.limit_results.subscribe(function (new_limit) {
    prefs.save(PREF_KEY_LIMIT_RESULTS, new_limit);
});

cityViewModel.rolls.subscribe(function (new_rolls) {
    prefs.save(PREF_KEY_ROLLS, new_rolls);
});

cityViewModel.name_rolls.subscribe(function (new_rolls) {
    prefs.save(PREF_KEY_NAMES_ROLLS, new_rolls);
});

cityViewModel.names_gender.subscribe(function (new_gender) {
    prefs.save(PREF_KEY_NAMES_GENDER, new_gender);
});

function close_theme_chooser() {
    cityViewModel.open_theme_chooser(false);
}

cityViewModel.open_theme_chooser.subscribe(function (is_opening) {
    if (is_opening) {
        document.body.addEventListener("click", close_theme_chooser);
    } else {
        document.body.removeEventListener("click", close_theme_chooser);
    }
});

preload_issue(cityViewModel.issue(), true);
reset_post_issue();
namegen.preload_names();

ko.applyBindings(cityViewModel);