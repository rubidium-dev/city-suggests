import { names } from "../data/names.json";
import * as suggest from "./suggest";

const DEFAULT_ROLLS = 20;

const SLOT_PREFIX = 1;
const SLOT_ONE = 2;
const SLOT_TWO = 4;
const SLOT_SUFFIX = 8;

const GENDER_MALE = "m";
const GENDER_FEMALE = "f";
const GENDER_BOTH = "";

const PREFIX_CHANCE = 33;
const SLOT_ONE_CHANCE = 80;
const CONJ_CHANCE = 8;
const COMPOUND_CHANCE = 12;
const SUFFIX_OR_MOD_CHANCE = 20;

const MAX_NAME_CHAR_LENGTH = 20; // matches CoH policy

let global_names = [];

function arr_rand(arr) {
    return arr && arr.length ? arr[suggest.rand(arr.length)] : "";
}

function chance(target) {
    return suggest.rand(100) < target;
}

function match_slot(value, slot) {
    return (value & slot) === slot;
}

function match_gender(value, gender) {
    if (value && gender) {
        return gender === value;
    }
    return true;
}

function match_themes(value, theme_one, theme_two) {
    if (value === theme_one) {
        return true;
    }
    if (value === theme_two) {
        return true;
    }
    return !(theme_one && theme_two);
}

export function preload_names() {
    global_names = names.map(name => {
        return Object.assign({ th: "", gender: "", slot: (SLOT_ONE | SLOT_TWO) }, name);
    });
}

export function roll_random_names(options) {
    // 1. Check options
    options = Object.assign({ rolls: DEFAULT_ROLLS, gender: GENDER_BOTH }, options);

    // 2. Set up results object, populate user themes if chosen
    let result = {
        "pri_theme": options.pri_theme || { "th": "" },
        "sec_theme": options.sec_theme || { "th": "" },
        "names": []
    };

    // 3. Build roll sets
    let build_name_map = function (slot, gender, no_themes) {
        return global_names.filter(name => (!!no_themes || match_themes(name.th, result.pri_theme.th, result.sec_theme.th)) &&
            match_slot(name.slot, slot) && match_gender(name.gender, gender)).flatMap(name => name.names);
    };

    let prefixes = {};
    prefixes[GENDER_MALE] = build_name_map(SLOT_PREFIX, GENDER_MALE, true);
    prefixes[GENDER_FEMALE] = build_name_map(SLOT_PREFIX, GENDER_FEMALE, true);

    let slot_ones = {};
    slot_ones[GENDER_MALE] = build_name_map(SLOT_ONE, GENDER_MALE);
    slot_ones[GENDER_FEMALE] = build_name_map(SLOT_ONE, GENDER_FEMALE);

    let slot_twos = {};
    if ((result.pri_theme.th || result.sec_theme.th) && !(result.pri_theme.th && result.sec_theme.th)) {
        // 3a. If the user left one of the themes as "any", ensure that slot two uses the specified theme
        let spec_theme = result.pri_theme.th || result.sec_theme.th;
        slot_twos[GENDER_MALE] = global_names.filter(name => match_themes(name.th, spec_theme, spec_theme) &&
            match_slot(name.slot, SLOT_TWO) && match_gender(name.gender, GENDER_MALE)).flatMap(name => name.names);
        slot_twos[GENDER_FEMALE] = global_names.filter(name => match_themes(name.th, spec_theme, spec_theme) &&
            match_slot(name.slot, SLOT_TWO) && match_gender(name.gender, GENDER_FEMALE)).flatMap(name => name.names);
    } else {
        slot_twos[GENDER_MALE] = build_name_map(SLOT_TWO, GENDER_MALE);
        slot_twos[GENDER_FEMALE] = build_name_map(SLOT_TWO, GENDER_FEMALE);
    }

    let suffixes = {};
    suffixes[GENDER_MALE] = build_name_map(SLOT_SUFFIX, GENDER_MALE, true);
    suffixes[GENDER_FEMALE] = build_name_map(SLOT_SUFFIX, GENDER_FEMALE, true);

    // 4. Roll individual names (uses a Set to automatically discard dupes)
    let names_set = new Set();
    while (names_set.size < options.rolls) {
        let name = [];

        // 4a. Determine what gender this entry will be
        let gender = options.gender ? options.gender : arr_rand(Array(GENDER_MALE, GENDER_FEMALE));

        // 4b. Should we roll a prefix?
        if (chance(PREFIX_CHANCE)) {
            name.push(arr_rand(prefixes[gender]));
        }

        // 4b. Should we roll a slot one?
        let compound = false;
        if (chance(SLOT_ONE_CHANCE)) {
            name.push(arr_rand(slot_ones[gender]));
            compound = true;
            // 4c. Should we include a conjunction?
            if (chance(CONJ_CHANCE)) {
                name.push(arr_rand(Array("and", "of")));
                compound = false;
            }
        }

        // 4d. Always roll a slot two
        name.push(arr_rand(slot_twos[gender]));
        // 4e. Compound?
        if (compound && chance(COMPOUND_CHANCE)) {
            let second = name.pop().toLocaleLowerCase();
            name.push(name.pop() + second);
        }

        // 4e. Should we apply a suffix/modifier?
        if (name.length === 1 || chance(SUFFIX_OR_MOD_CHANCE)) {
            // Choice between a modifier, a number, or a choice from the suffix list
            if (chance(50)) {
                let suffixes = (gender === GENDER_MALE ? Array("o", "er") : Array("a", "ette"))
                    .concat(["y", "izer", "ator", "ist", "ity", "ism", "ant", "age", "ery"]);
                name[name.length - 1] = name[name.length - 1].replace(/(ic|al|at|ing|ish|ism|ity|ed|ious|ous|able|er|izer|ation|[aeiouy]|)$/,
                    arr_rand(suffixes));
            } else if (chance(name.length === 1 ? 50 : 25)) {
                let number = arr_rand(Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
                    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve"));
                name.push(number);
            } else if (chance(50)) {
                name[name.length - 1] += arr_rand(["ment", "ness"]);
            } else {
                let suffix = arr_rand(suffixes[gender]);
                // avoid duping the prefix
                if (suffix !== name[0]) {
                    name.push(suffix);
                }
            }
        }

        // 4f. Combine slots into a name and check final length
        let name_str = name.join(" ");
        let name_len = name[0] === "The" ? name_str.length - 4 : name_str.length; // "the" can be prefixed in the game
        if (name_len <= MAX_NAME_CHAR_LENGTH) {
            names_set.add(name_str);
        }
    }

    result.names = [...names_set.values()];
    return result;
}