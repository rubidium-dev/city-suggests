import { archetypes } from "../data/archetypes.json";
import { powersets } from "../data/powersets.json";
import { themes } from "../data/themes.json";

const ISSUE_I24 = "i24";
const ISSUE_I25 = "i25";
const ISSUE_I26 = "i26";
const ISSUE_I27 = "i27";
const DEFAULT_ISSUE = ISSUE_I24;
const ALL_ISSUES = [ISSUE_I24, ISSUE_I25, ISSUE_I26, ISSUE_I27];
const DEFAULT_EXCLUDE = false;

let global_ats = [];
let global_sets = [];
let global_themes = [];

export function preload_issues(issue_latest) {
    let issues = ALL_ISSUES.slice(0, ALL_ISSUES.indexOf(issue_latest) + 1);
    if (issues.length === 0) {
        throw "Unknown issue specified: " + issue_latest;
    }

    global_ats = [];
    for (var at_base of archetypes) {
        let at_to_save = Object.assign({ issue: DEFAULT_ISSUE, exclude: DEFAULT_EXCLUDE }, at_base);
        if (issues.some(issue => issue === at_to_save.issue)) {
            global_ats.push(at_to_save);
        }
    }

    global_sets = [];
    for (var set_base of powersets) {
        let set_to_save = Object.assign({ issue: DEFAULT_ISSUE, exclude_set: [] }, set_base);
        if (!set_to_save.hasOwnProperty("themes")) {
            throw "Power set " + set_to_save.name + " missing theme(s)";
        }
        if (global_ats.some(at => at.at === set_to_save.at) && issues.some(issue => issue === set_to_save.issue)) {
            global_sets.push(set_to_save);
        }
    }

    global_themes = [];
    for (var theme_base of themes) {
        let theme_to_save = Object.assign({ exclude: DEFAULT_EXCLUDE }, theme_base);
        global_themes.push(theme_to_save);
    }
}

function sets_for_at(at_id, pri_or_sec) {
    let apri = typeof pri_or_sec === "undefined" ? "pri" : pri_or_sec;
    return global_sets.filter(set => set.at === at_id && set.slot === apri);
}

export function get_all_ats() {
    return [...global_ats];
}

export function get_all_themes() {
    return [...global_themes].sort((a, b) => {
        if (a.name < b.name) {
            return -1;
        }
        if (a.name > b.name) {
            return 1;
        }
        return 0;
    });
}

export function get_all_sets() {
    return [...global_sets];
}

function get_included_ats() {
    return global_ats.filter(at => !at.exclude);
}

function get_included_themes() {
    return global_themes.filter(th => !th.exclude);
}

function get_at(at_id) {
    return global_ats.find(at => at.at === at_id);
}

function get_theme(th_id) {
    return global_themes.find(theme => theme.th === th_id);
}

export function set_exclude_at(at_id, exclude) {
    for (var at of global_ats) {
        if (at.at === at_id) {
            at.exclude = exclude;
        }
    }
}

export function set_exclude_theme(th_id, exclude) {
    for (var th of global_themes) {
        if (th.th === th_id) {
            th.exclude = exclude;
        }
    }
}

export function reset_excluded_themes() {
    for (var th of global_themes) {
        th.exclude = false;
    }
}

export function rand(max) {
    return Math.floor(Math.random() * max);
}

export function roll_a_random_at_and_sets() {
    let ats = get_included_ats();
    if (ats.length === 0) {
        throw "All archetypes have been excluded.";
    }
    let at = ats[rand(ats.length)];
    let pri_sets = sets_for_at(at.at);
    let sec_sets = sets_for_at(at.at, "sec");
    let pri = pri_sets[rand(pri_sets.length)];
    let sec = sec_sets[rand(sec_sets.length)];
    return {
        "archetype": at,
        "sets": [{
            "primary": pri,
            "secondary": sec
        }]
    };
}

export function roll_random_themes(options) {
    // 1. Check options
    options = Object.assign({ suggest_only_one_set: true }, options);

    // 2. Roll a random primary and secondary theme
    //    If pri_theme/sec_theme were passed in, use those instead
    let themes = get_included_themes();
    if (themes.length === 0 && !(options.pri_theme && options.sec_theme)) {
        throw "All themes have been excluded.";
    }
    let result = {
        "pri_theme": (options.pri_theme && get_theme(options.pri_theme)) || themes[rand(themes.length)],
        "sec_theme": (options.sec_theme && get_theme(options.sec_theme)) || themes[rand(themes.length)],
        "archetypes": []
    };

    // 3. Pull all of the power sets for each rolled theme
    let pri_sets = global_sets.filter(set =>
        set.slot === "pri" && set.themes.some(theme => theme === result.pri_theme.th));
    let sec_sets = global_sets.filter(set =>
        set.slot === "sec" && set.themes.some(theme => theme === result.sec_theme.th));

    // 4a. Iterate each primary set
    for (var pri_set of pri_sets) {
        let at = get_at(pri_set.at);
        if (!at.exclude) {
            // 4b. Have we seen this archetype yet? If so, add to the existing node, otherwise create a new one.
            let existing_at = result.archetypes.find(found => found.archetype.at === at.at);
            if (typeof existing_at === "undefined") {
                existing_at = {
                    "archetype": at,
                    "sets": []
                };
                result.archetypes.push(existing_at);
            }
            // 4c. Find each valid pri/sec combo for the archetype and add it to the list
            for (var sec_set of sec_sets) {
                if (pri_set.at === sec_set.at) {
                    // 4d. Make sure the sets are compatible with each other
                    if (pri_set.exclude_set.indexOf(sec_set.name) === -1 &&
                        sec_set.exclude_set.indexOf(pri_set.name) === -1) {
                        existing_at.sets.push({
                            "primary": pri_set,
                            "secondary": sec_set
                        });
                    }
                }
            }
        }
    }

    // 5. Remove archetype nodes that didn't result in any valid power set combinations
    result.archetypes = result.archetypes.filter(at => at.sets.length > 0);

    // 6. If the user only wants to see one suggestion per archetype, choose one of the power set combos at random for each archetype
    if (options.suggest_only_one_set) {
        result.archetypes.forEach(at => {
            at.sets = [at.sets[rand(at.sets.length)]];
        });
    }

    return result;
}
