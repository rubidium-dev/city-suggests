import "../sass/tooltips.scss";

var tooltip, window;
let opened_by_mouse = false;
let opened_by_click = false;

const DEFAULT_OFFSET = 6;
const TOOLTIP_ATTR = "data-tooltip";

function tooltip_mouseenter(e) {
    opened_by_mouse = true;
    opened_by_click = false;
    tooltip_display(e);
}

function tooltip_mouseleave(e) {
    if (opened_by_mouse) {
        opened_by_mouse = false;
        tooltip_hide(e);
    }
}

function tooltip_click(e) {
    if (opened_by_click) {
        opened_by_click = false;
        tooltip_hide(e);
    } else {
        opened_by_click = true;
        opened_by_mouse = false;
        tooltip_display(e);
    }
    e.stopPropagation();
}

function tooltip_display(e) {
    let target_rect = e.target.getBoundingClientRect();
    let tip = e.target.getAttribute(TOOLTIP_ATTR);
    if (tip) {
        tooltip.innerHTML = tip;
        let tooltip_rect = tooltip.getBoundingClientRect();
        let view_width = window.document.documentElement.clientWidth || window.document.body.clientWidth;
        let new_left = window.scrollX + target_rect.right + DEFAULT_OFFSET;
        if ((new_left + tooltip_rect.width) > view_width) {
            new_left = view_width - tooltip_rect.width - DEFAULT_OFFSET;
        }
        tooltip.style.left = (new_left) + "px";
        tooltip.style.top = (window.scrollY + target_rect.bottom + DEFAULT_OFFSET) + "px";
        tooltip.style.visibility = "visible";
    }
}

function tooltip_hide(e) {
    tooltip.style.visibility = "hidden";
}

export function init(window_obj, selector) {
    if (window_obj && window_obj.document) {
        window = window_obj;
        if (typeof tooltip === "undefined") {
            tooltip = window.document.createElement("div");
            tooltip.className = "tooltip";
            window.document.body.appendChild(tooltip);
            window.document.body.addEventListener("click", tooltip_hide);
        }
        add(selector);
    }
}

export function add(selector) {
    if (window && selector) {
        let nodes = window.document.querySelectorAll(selector);
        for (var node of nodes) {
            let tip = node.getAttribute("title");
            if (tip) {
                node.setAttribute(TOOLTIP_ATTR, tip);
                node.removeAttribute("title");
                node.addEventListener("mouseenter", tooltip_mouseenter);
                node.addEventListener("mouseleave", tooltip_mouseleave);
                node.addEventListener("click", tooltip_click);
            }
        }
    }
}