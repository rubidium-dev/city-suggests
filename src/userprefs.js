
export function get(key, default_value) {
    if (window && window.localStorage && key) {
        let value = window.localStorage.getItem(key);
        if (value !== null) {
            return value;
        }
    }
    return default_value;
}

export function get_bool(key, default_value) {
    let value = get(key, null);
    if (value !== null) {
        return value === "true";
    }
    return !!default_value;
}

export function get_int(key, default_value) {
    let value = get(key, null);
    if (value !== null) {
        value = parseInt(value, 10);
        if (value !== NaN) {
            return value;
        }
    }
    return Number.isInteger(default_value) ? default_value : 0;
}

export function get_obj(key, default_value) {
    let value = get(key, null);
    if (value !== null) {
        try {
            return JSON.parse(value);
        } catch (e) {
            console.error(e);
        }
    }
    return default_value;
}

export function save(key, value) {
    if (window && window.localStorage && key) {
        if (typeof value === "undefined" || value === null || value === NaN) {
            window.localStorage.removeItem(key);
        } else {
        window.localStorage.setItem(key, value);
        }
    }
}

export function save_obj(key, value) {
    if (typeof value === "object") {
        value = JSON.stringify(value);
    }
    save(key, value);
}