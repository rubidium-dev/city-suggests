const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    "mode": "production",
    "entry": {
        "model": "./src/model.js",
        "tooltips": "./src/tooltips.js"
    },
    "output": {
        "filename": "[name].js",
        "path": path.resolve(__dirname, "dist"),
        "libraryTarget": "var",
        "library": "[name]"
    },
    "devServer": {
        "contentBase": path.join(__dirname, "dist"),
        "compress": true,
        "port": 9000
    },
    "plugins": [
        new MiniCssExtractPlugin({
            "filename": "[name].css",
        }),
    ],
    "module": {
        "rules": [{
            "test": /\.scss$/,
            "use": [
                {
                    "loader": MiniCssExtractPlugin.loader
                },
                "css-loader",
                {
                    "loader": "sass-loader",
                    "options": {
                        "sassOptions": {
                            "includePaths": [
                                "./node_modules/normalize-scss/sass"
                            ],
                            "outputStyle": "compressed"
                        }
                    }
                }
            ]
        },
        {
            "test": /\.(woff(2)?|ttf|otf|eot)$/,
            "use": [{
                "loader": "file-loader",
                "options": {
                    "name": "[name].[ext]",
                    "outputPath": "assets/fonts"
                }
            }]
        },
        {
            "test": /\.(png|jpg|gif)$/,
            "use": [{
                "loader": "file-loader",
                "options": {
                    "name": "[name].[ext]",
                    "outputPath": "assets/images"
                }
            }]
        }]
    },
    "performance": {
        "hints": false,
        "maxEntrypointSize": 512000,
        "maxAssetSize": 512000
    }
}